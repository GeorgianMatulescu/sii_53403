// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta
{
public:
	Vector2D velocidad;
	Plano plano;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
	void Dibuja();
};
