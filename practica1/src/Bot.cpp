#include <math.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include "DatosMemCompartida.h"

int main(int argc, char **argv){
	DatosMemCompartida *bot;
	int fd;
	float coord_y_esfera, coord_y1_raq, coord_y2_raq;
	float pto_medio_raq;
	struct stat st;
	
	fd = open("DatosMemoriaCompartida.txt", O_RDWR);
	fstat(fd, &st);
	bot = static_cast<DatosMemCompartida*>(mmap(NULL, st.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
	close(fd);
	
	while(1){
		if(bot->fin == true){
			munmap(bot, st.st_size);
			//printf("Bot se retira\n");
			return 0;
		}
			
		coord_y_esfera = bot->esfera.centro.y;
		coord_y1_raq = bot->raqueta2.plano.y1;
		coord_y2_raq = bot->raqueta2.plano.y2;
		
		pto_medio_raq = (coord_y1_raq + coord_y2_raq)/2;
		
		if((pto_medio_raq - coord_y_esfera) >= 0)
			bot->accion = 0; //abajo
		else
			bot->accion = 3; //arriba
		
		usleep(25000);
	}
	return 0;
}