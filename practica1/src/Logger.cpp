#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_BUFFER 100

int main(int argc, char **argv){
	int fd;
	int n_bytes;
	char buffer[MAX_BUFFER];
	pid_t pid;
	
	mkfifo("/tmp/mi_fifo", 0666);
	
	pid = fork();
	if (pid == -1){
		perror("Error al crear el fork\n");
		return 0;
	}
	else if (pid == 0)
		execvp("./tenis", argv);
	else{
		fd = open("/tmp/mi_fifo", O_RDONLY);
		while (1){
			while ((n_bytes = read(fd, buffer, MAX_BUFFER)) > 0){
				if((n_bytes == 1) && (buffer[0] == 'f')){
					close(fd);
					unlink("/tmp/mi_fifo");
					//printf("Hasta pronto\n");
					exit(0);
				}
				else{
					write(1, buffer, n_bytes);
					char puntos[] = " puntos.\n";
					write(1, puntos, sizeof(puntos));
				}
			}
		}
	}
	
	return 0;
}